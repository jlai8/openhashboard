# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
#########################################################################

import tweepy
import json

access_token = ""  ##NOT PUBLIC EMAIL FOR KEY
access_token_secret = ""  ##NOT PUBLIC EMAIL FOR KEY
consumer_key = ""  ##NOT PUBLIC EMAIL FOR KEY
consumer_secret = ""  ##NOT PUBLIC EMAIL FOR KEY

def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    pastsearches = db(db.searches).select()
    return dict(pastsearches = pastsearches)

@auth.requires_login()
def boardsettings():
    """
    Allow the user to change display settings of their board.
    Background_image: allow the user to change their background image
    Refresh Interval: Change how many seconds to wait before requesting new posts
    Post Width: How long is one post width, in px
    Show Header: enable/disable showing the header
    Twitter Enable: To show twitter posts? Just disregard this for now.
    """
    pastsearches = db(db.searches).select()
    db.user_settings.user_id.readable = db.user_settings.user_id.writable = False
    form = SQLFORM(db.user_settings)
    userdata = db((db.user_settings.user_id == auth.user_id)).select().last()
    if userdata is not None:
        form.vars.user_id = auth.user_id
        form.vars.background_image = userdata.background_image
        form.vars.refresh_interval = userdata.refresh_interval
        form.vars.post_width = userdata.post_width
        form.vars.show_header = userdata.show_header
        form.vars.twitter_enable = userdata.twitter_enable
        form.vars.logo_name = userdata.logo_name
        form.vars.logo_image = userdata.logo_image
        form.vars.show_logo_on_board = userdata.show_logo_on_board
    if form.process().accepted:
        if userdata is not None:
            del db.user_settings[userdata.id]
        redirect(URL('default', 'index'))
    return dict(form=form, pastsearches = pastsearches)

@auth.requires_login()
def tbs():
    """
    Just some user data to check user
    :return:
    """
    userdata = db((db.user_settings.user_id == auth.user_id))
    userdatas = userdata.select()
    return dict(user = userdatas)

@auth.requires_login()
def removefirst():
    """
    TEMP TO CLEAR OUT OLD THINGS
    """
    userdata = db((db.user_settings.user_id == auth.user_id))
    userdatas = userdata.select().first().id
    del db.user_settings[userdatas]
    return dict(user = "ok")

@auth.requires_login()
def fillboardbackend():
    """
    TEMP TO APPEND SEARCHES FOR BACKGOURND DISPLAY
    """
    temp = request.args(0)
    fav = request.args(1)
    if temp is not None:
        if fav is not None:
            db.searches.insert(
                search_string = temp,
                favorite = True,
            )
        else:
           db.searches.insert(
                search_string = temp,
            )


@auth.requires_login()
def savehash():
    """
    STAR/FAVORITE THE HASH SEARCHES FOR LATER REFRENCE. WHEN THE USER
    IS IN THE SEARCH FIELDS, HE WILL SEE A LIST OF FAVORITES
    """
    searchvalue = request.args(0)
    varmode = request.args(1)
    if varmode is 's':
        pastsearches = db((db.searches.search_string == searchvalue) & (db.searches.user_id == auth.user_id) & (db.searches.favorite == False)).select().last()
        db.searches[pastsearches.id] = dict(favorite = True)
    else:
        pastsearches = db((db.searches.search_string == searchvalue) & (db.searches.user_id == auth.user_id) & (db.searches.favorite == True)).select().last()
        db.searches[pastsearches.id] = dict(favorite = False)
    return 'ok'


@auth.requires_login()
def search():
    """
    LAUNCH THE SEARCHES FOR A CERTAIN HASH.
    IN PROGRESS TO INTEGRATE TWITTER API FUNCTIONS TO IT
    """
    searchtag = request.args(0)
    if searchtag is None:
        redirect(URL('default', 'index'))
    searchdatabase = db((db.searches.search_string == searchtag) &
                        (db.searches.user_id == auth.user_id)).select().last()
    if searchdatabase is None:
        db.searches.insert(
            search_string = searchtag,
        )
        searchdatabase = db((db.searches.search_string == searchtag) &
                            (db.searches.user_id == auth.user_id)).select().last()
    userdata = db((db.user_settings.user_id == auth.user_id)).select().last()
    query = '%23' + request.args(0)
    authtwitter = tweepy.OAuthHandler(consumer_key, consumer_secret)
    authtwitter.secure = True
    authtwitter.set_access_token(access_token, access_token_secret)
    api= tweepy.API(authtwitter, parser=tweepy.parsers.JSONParser())
    results = api.search(q=query, count=5)
    return dict(searchtag = searchtag, searchdatabase = searchdatabase, results = results, userdata = userdata)

@auth.requires_login()
def searchbackup():
    """
    OLD JUNK FOR BACKUP REFRENCE
    """
    searchtag = request.args(0)
    if searchtag is None:
        redirect(URL('default', 'index'))
    db.searches.insert(
        search_string = searchtag,
    )
    return dict(searchtag = searchtag)

def createsettingsregister(fields, id):
    """
    When the user registers with the website, create a settings for the user with the default
    settins we prescribe
    """
    db.user_settings.insert(
        user_id = id,
        background_image = "",
        refresh_interval = 300,
        post_width = 200,
        show_header = True,
        twitter_enable = True,
        logo_name = "OpenHashBoard",
        show_logo_on_board = True
    )


def tmpsearch():
    """
    OLD JUNK FOR BACKUP REFRENCE
    DISREGARD THIS
    """
    temp = db((db.searches.search_string == request.args(0)) & (db.searches.user_id == auth.user_id)).select().last()
    return dict(temp = temp)

def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    pastsearches = db(db.searches).select()
    db.auth_user._after_insert.append(createsettingsregister) # check this for me later CHECK
    return dict(form=auth(), pastsearches = pastsearches)


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


