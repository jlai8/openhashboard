import tweepy
import json

access_token = "" ##NOT PUBLIC EMAIL FOR KEY
access_token_secret = "" ##NOT PUBLIC EMAIL FOR KEY
consumer_key = "" ##NOT PUBLIC EMAIL FOR KEY
consumer_secret = "" ##NOT PUBLIC EMAIL FOR KEY


def testgetprofile():
    """
    TEMP TO TEST OUT API BY SEARCHING PROFILE INFO
    """
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.secure = True
    auth.set_access_token(access_token, access_token_secret)
    api = tweepy.API(auth)
    temp = api.get_user(id='TheDailyShow')
    return dict(temp = temp)

def testsearchtweets():
    """

    """
    if request.args(0) is None:
        redirect(URL('default', 'index'))
    query = request.args(0)
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.secure = True
    auth.set_access_token(access_token, access_token_secret)
    api= tweepy.API()
    searched_tweets = tweepy.Cursor(api.search,
                           q=query,
                           rpp=100,
                           result_type="recent",
                           include_entities=True,
                           lang="en").items()
    return dict(searched_tweets = searched_tweets)

def testsearch():
    if request.args(0) is None:
        redirect(URL('default', 'index'))
    query = request.args(0)
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.secure = True
    auth.set_access_token(access_token, access_token_secret)
    api= tweepy.API(auth, parser=tweepy.parsers.JSONParser())
    results = api.search(q=query, count=5)
    return dict(results = results)

def testsearch2():
    if request.args(0) is None:
        redirect(URL('default', 'index'))
    query = "#" + request.args(0)
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.secure = True
    auth.set_access_token(access_token, access_token_secret)
    api= tweepy.API(auth, parser=tweepy.parsers.JSONParser())
    results = api.search(q=query, count=100)
    return dict(results = results, searchstring = query)

def gettweet():
    """
    Get single tweet for showing one post only
    """
    if request.args(0) is None:
        redirect(URL('default', 'index'))
    query = request.args(0)
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.secure = True
    auth.set_access_token(access_token, access_token_secret)
    api= tweepy.API(auth, parser=tweepy.parsers.JSONParser())
    tweet = api.get_status(id=query)
    return dict(tweet = tweet)
