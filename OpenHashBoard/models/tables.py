#########################################################################
## Define your tables below; for example
##
## >>> db.define_table('mytable',Field('myfield','string'))
##
## Fields can be 'string','text','password','integer','double','boolean'
##       'date','time','datetime','blob','upload', 'reference TABLENAME'
## There is an implicit 'id integer autoincrement' field
## Consult manual for more options, validators, etc.
##
## More API examples for controllers:
##
## >>> db.mytable.insert(myfield='value')
## >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
## >>> for row in rows: print row.id, row.myfield
#########################################################################

from datetime import datetime

# this is a table for user settings
db.define_table('user_settings',
                Field('user_id', db.auth_user, default=auth.user_id),
                Field('background_image', 'string'),
                Field('refresh_interval', 'list:integer', default=0),
                Field('post_width', 'integer', default=200),
                Field('show_header', 'boolean', default=True),
                Field('twitter_enable', 'boolean', default=True),
                Field('logo_name', 'string', default="OpenHashBoard"),
                Field('logo_image', 'string'),
                Field('show_logo_on_board', 'boolean', default=True)
                )

db.user_settings.background_image.label = "Background Image"
db.user_settings.background_image.comment = "Please specify the URL for the background image"
db.user_settings.refresh_interval.label = "Refresh Interval"
db.user_settings.refresh_interval.comment = "In number of seconds (0 for disabled)"
db.user_settings.post_width.label = "Post Width"
db.user_settings.post_width.comment = "How Long is the width of each post (in px)"
db.user_settings.show_header.label = "Show heading"
db.user_settings.show_header.comment = "Show the heading on top of the board?"
db.user_settings.refresh_interval.requires = IS_IN_SET((0, 30, 60, 90, 120, 180, 240, 300))
db.user_settings.refresh_interval.default = 4
db.user_settings.twitter_enable.label = "Show Twitter Posts"
db.user_settings.logo_name.label = "Logo Name Shown On Board"
db.user_settings.logo_image.label = "Logo Image"
db.user_settings.logo_image.comment = "Please specify the URL for the logo image"
db.user_settings.show_logo_on_board.label = "Show Logo On Board"


db.define_table('searches',
                 Field('user_id', db.auth_user, default=auth.user_id),
                 Field('search_string', 'string'),
                 Field('favorite', 'boolean', default=False)
                 )
db.searches.user_id.writable = db.searches.user_id.readable = False